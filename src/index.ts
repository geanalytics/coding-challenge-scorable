import { Database } from "./config/Database";
import { Logging } from "./config/Logging";
import { AlertNotification } from "./entity/AlertNotification";

new Database()
    .getConnection()
    .then(async connection => {
        const alertNotifications = await connection.getRepository(AlertNotification).find();
        Logging.getLogger().info(alertNotifications);
        await connection.close();
    })
    .catch(error => {
        Logging.getLogger().error(error);
    });
