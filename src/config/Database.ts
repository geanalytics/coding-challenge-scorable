import { Connection, ConnectionManager, ConnectionOptions, createConnection, getConnectionManager } from "typeorm";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";
import { DB_CONFIG } from "../../config";
import { Logging } from "./Logging";

/**
 * Database manager class
 */
export class Database {
    private connectionManager: ConnectionManager;

    constructor() {
        this.connectionManager = getConnectionManager();
    }

    public async getConnection(): Promise<Connection> {
        const CONNECTION_NAME = "default";
        let connection: Connection;
        if (this.connectionManager.has(CONNECTION_NAME)) {
            Logging.getLogger().info("using existing connection ...");
            connection = await this.connectionManager.get(CONNECTION_NAME);

            if (!connection.isConnected) {
                connection = await connection.connect();
            }
        } else {
            Logging.getLogger().info("creating connection ...");
            const connectionOptions: ConnectionOptions = {
                database: DB_CONFIG.database,
                entities: [require("path").resolve(__dirname, "..") + "/entity/*.*"],
                host: DB_CONFIG.host,
                logging: false,
                name: "default",
                namingStrategy: new SnakeNamingStrategy(),
                password: DB_CONFIG.password,
                port: DB_CONFIG.port,
                synchronize: false,
                type: "postgres",
                username: DB_CONFIG.username
            };

            connection = await createConnection(connectionOptions);
        }
        return connection;
    }
}
