import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class AlertNotification {
    @PrimaryGeneratedColumn("increment")
    public readonly id: number;
    @Column()
    public readonly userId: number;
    @Column()
    public readonly type: string;
    @Column()
    public readonly fromValue: string;
    @Column()
    public readonly toValue: string;
    @Column()
    public readonly isRead: boolean;
    @Column()
    public readonly creationDate: Date;
}
