create table alert_notification
(
    id            bigserial not null primary key,
    user_id       bigint    not null,
    type          text      not null,
    from_value    text,
    to_value      text      not null,
    is_read       boolean   not null,
    creation_date date      not null
);
